;/* LOS     = 1.0.0    LAST_UPDATE = 1.0.0    */
;
;    Title :  Losjmp.asm
;    Author:  Linda Wheeler
;    Date  :  September 12, 1986
;
;    The following logical o/s interface routines
;    push the appropriate logical o/s function
;    code onto the stack and call los_jump
;
; define logical o/s function codes
;
ENQM equ  0
DEQM equ  1
CRET equ  2
SETT equ  3
DELT equ  4
CLRT equ  5
CHKT equ  6
RESUME    equ  7
PREEMPT equ    8
SLEEP     equ  9
SUSPEND equ    10
GETM equ  18
FREM equ  19
;
PUBLIC    _ENQ_TASK
PUBLIC    _DEQ_TASK
PUBLIC    _CREATE_TIMER
PUBLIC    _SET_TIMER
PUBLIC    _DELETE_TIMER
PUBLIC    _CLEAR_TIMER
PUBLIC    _CHECK_TIMER
PUBLIC    _RESUME
PUBLIC    _PREEMPT
PUBLIC    _SLEEP
PUBLIC    _SUSPEND
;PUBLIC _GET_MEMORY    RDC removed 4/15/87
;PUBLIC _FREE_MEMORY   RDC removed 4/15/87 see comments below
;
_TEXT     SEGMENT BYTE PUBLIC 'CODE'
     ASSUME CS:_TEXT
;
_ENQ_TASK PROC FAR
          mov  ax,ENQM
          push ax
          jmp  FAR PTR _los_jump
_ENQ_TASK ENDP
;
_DEQ_TASK PROC FAR
          mov  ax,DEQM
          push ax
          jmp  FAR PTR _los_jump
_DEQ_TASK ENDP
;
_CREATE_TIMER  PROC FAR
          mov  ax,CRET
          push ax
          jmp  FAR PTR _los_jump
_CREATE_TIMER  ENDP
;
_SET_TIMER     PROC FAR
          mov  ax,SETT
          push ax
          jmp  FAR PTR _los_jump
_SET_TIMER     ENDP
;
_DELETE_TIMER  PROC FAR
          mov  ax,DELT
          push ax
          jmp  FAR PTR _los_jump
_DELETE_TIMER  ENDP
;
_CLEAR_TIMER   PROC FAR
          mov  ax,CLRT
          push ax
          jmp  FAR PTR _los_jump
_CLEAR_TIMER   ENDP
;
_CHECK_TIMER   PROC FAR
          mov  ax,CHKT
          push ax
          jmp  FAR PTR _los_jump
_CHECK_TIMER   ENDP
;
_RESUME   PROC FAR
          mov  ax,RESUME
          push ax
          jmp  FAR PTR _los_jump
_RESUME   ENDP
;
_PREEMPT  PROC FAR
          mov  ax,PREEMPT
          push ax
          jmp  FAR PTR _los_jump
_PREEMPT  ENDP
;
_SLEEP         PROC FAR
          mov  ax,SLEEP
          push ax
          jmp  FAR PTR _los_jump
          ret
_SLEEP         ENDP
;
_SUSPEND  PROC FAR
          mov  ax,SUSPEND
          push ax
          jmp  FAR PTR _los_jump
_SUSPEND  ENDP
;
;*******************************************************
; RDC 4/15/87 Removed these to facilitate redirection of comm
; manager's memory allocation call.
;
;_GET_MEMORY   PROC FAR
;         mov  ax,GETM
;         push ax
;         jmp  FAR PTR _los_jump
;_GET_MEMORY   ENDP
;
;_FREE_MEMORY  PROC FAR
;         mov  ax,FREM
;         push ax
;         jmp  FAR PTR _los_jump
;_FREE_MEMORY  ENDP
;
;
; ************ END RDC 4/15/87 CHANGES ******************
;
;
;    The logical operating system can be located by examining the
;    7A interrupt vector and making a far rtn to that location + 16h.
;
_los_jump PROC FAR            ; need to force far return.
     PUSH DS             ; save
     XOR  AX,AX               ; clear AX
     MOV  DS,AX               ; first page in memory.
     MOV  BX,7AH*4       ; offset vector address.
     MOV  AX,[BX+2]      ; get TMK segment.
     MOV  BX,[BX]        ; get TMK offset.
     ADD  BX,16h              ; add offset into TMK.
     POP  DS             ; restore ds
     PUSH AX             ; push TMK segment
     PUSH BX             ; push TMK offset
     RET                 ; return to jump of LOSGO.
_los_jump ENDP
;
_TEXT     ENDS
END
