;/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
;********************************************************************/
;*                                          */
;* IWAQSVCS - send SVC request to multitasker                   */
;*                                          */
;*    DESCRIPTION - send an SVC request. First set the registers */
;*        required and execute the software interrupt. Upon     */
;*        return, return to the caller. The caller may end up    */
;*        suspended within this routine, depending on the type   */
;*        of request. The code is reentrant, so that does not    */
;*        cause problems, provided each invoking task has its own*/
;*        stack.                                 */
;*                                          */
;*    HOW INVOKED - via call of issuesvc with the form     */
;*             issuesvc(&plist,&reglist)              */
;*           or     result = issuesvc(p1,r1)          */
;*                                          */
;*    INPUTS -                              */
;*        PARAMETERS:                            */
;*        offset: address of parmlist data. In small model,   */
;*             the offset in "ss" of the data. Zero is ok. */
;*        reglist: pointer in stack seg. to ax,bx,cx for svc  */
;*           in that order. AX = SFV index, BX = request type,*/
;*           CX = specification value that varies with type.  */
;*        SVC Request data / Plist:                        */
;*        The data (or parameter list) varies with the given  */
;*          SVC.                                 */
;*                                          */
;*    OUTPUTS -                                  */
;*        Registers ax,bx,cx,dx, and si are modified here.      */
;*          If the caller treats this routine as a function,  */
;*          it gets AX as the result. For some SVCs that is   */
;*          a useful value.                      */
;*                                          */
;*    COMMENTS -                                 */
;*        1. Interrupt 7A is used to trigger SVC.          */
;*                                          */
;********************************************************************/
     include prolog.h
     public    _issuesvc
ifdef     @BIGMODEL
plst equ  @ab
rlst equ  plst+4
_issuesvc proc   far
else
plst equ  @ab
rlst equ  plst+2
_issuesvc proc   near
endif
     push bp          ;save bp
     mov  bp,sp            ;get parameters...
     push es          ;save es
     push si          ;client may need si saved
     mov  dx,ss            ;(cannot do mov es,ss directly)
     mov  es,dx            ;the segment of the svc plist
     mov  dx,plst[bp]      ;... and its offset
     mov  si,rlst[bp]      ;... and the qsvc regs plist
     mov  ax,es:0[si]      ;load the sfvindex into ax
     mov  bx,es:2[si]      ;... the svc request type into bx
     mov  cx,es:4[si]      ;... the special value into cx
     int  7ah         ;execute the svc
     pop  si          ;
     pop  es          ;restore es
     pop  bp          ;restore bp
     ret              ;return to caller
_issuesvc endp
     include epilogue.h
;**********************************************************************/
;*     END IWAQSVCS                              */
;**********************************************************************/
     end