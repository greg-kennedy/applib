;/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
;********************************************************************/
;*                                          */
;* IWADRET  - return to multitasker dispatcher                  */
;*                                          */
;*    DESCRIPTION - return to the dispatcher with either a      */
;*        "dispatchable" or "nondispatchable" state.             */
;*                                          */
;*    HOW INVOKED - via call of dispret with one or three args.  */
;*             dispret(status)                   */
;*           or     dispret(_QUSETWT,timerofst,timerseg)        */
;*                                          */
;*    INPUTS -                              */
;*        PARAMETERS:                            */
;*        status: disp/nodisp status to use. See system         */
;*             equates "_QDISP," "_QNODISP," and "_QUSET." */
;*             Also "_QUSETWT" which is local only.        */
;*                                          */
;*    OUTPUTS -                                  */
;*        Registers are preserved. Function returns no data.     */
;*                                          */
;*    COMMENTS -                                 */
;*        1. Interrupt 79 is used to trigger dispatcher.        */
;*        2. QUSETWT means QUSET with a timer specified.        */
;*                                          */
;********************************************************************/
     include prolog.h
     public    _dispret
ifdef     @BIGMODEL
_dispret proc   far
else
_dispret proc   near
endif
     push bp          ;save bp
     mov  bp,sp            ;get parameter
     push ax
     push bx
     push dx
     push es          ;save es
     mov  ax,@ab[bp]       ;dispatch status
     cmp  ax,4        ;if qusetwt
     jne  notime
     mov  ax,3        ;convert to uset
     mov  bx,@ab+4[bp]     ;timer segment
     mov  es,bx
     mov  bx,@ab+2[bp]     ;timer offset
     jmp  com
notime: xor    bx,bx            ;no timer
     mov  es,bx            ;
com: mov  dx,ds            ;save data seg
     int  79h         ;signal dispatcher
     mov  ds,dx            ;restore ds
     pop  es          ;
     pop  dx          ;
     pop  bx          ;
     pop  ax          ;
     pop  bp          ;restore bp
     ret              ;return to caller
_dispret endp
     include epilogue.h
;**********************************************************************/
;*     END IWADRETS                              */
;**********************************************************************/
     end