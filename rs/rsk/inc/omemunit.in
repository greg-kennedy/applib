/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/* omemunit.in 8/2/87 11:20 */
/*   
     file:          omemunit.in
     author:        Michael L. Gordon
     date:          1-August-1987
     facility: Object Management Memory Facility


     Description:

          Public interface of the object memory unit.

     History:

*/
#ifndef   omemunitDEF
#define omemunitDEF 1

#ifndef OBJUNITDEF
#include  <objunit.in>
#endif

/* definition of context structure for memory buffer search and update
   functions
*/
     typedef struct {
          Objidxt        objidx;
          Wordt          status;
     } OSearchContext;
/* definition of the status field of the OSearchContext structure */
#define   oscstStart          ( Wordt ) 0
#define   oscstAccessList     ( Wordt ) 1
#define   oscstCacheList      ( Wordt ) 2
#define   oscstEnd            ( Wordt ) 3

/* function OMemUsageFirstToLast
     This function will return memory pointers of objects in the following
     order:
          the accessed objects in directory order ( not usage order )
          then the memory cached objects in most to least recently used order

     The searchContext structure is updated by the function in order to
     maintain search context between activations.

     This function behaves accurately only if the object directory is not
     altered between function activations.  It is safe to invoke the
     OMemAdrChange function, however.

     Start value:
          The searchContext structure's status field should be set to
          oscstStart to prepare for the start of search.

     Returns:
          If memory pointer obtained:
               returns the address of the buffer
               contextStructure updated with directory reference suitable
                 for OMemAdrChange function activation.
          If no memory pointer obtained:
               returns the nil pointer
               contextStructure contains no directory referecne

extern    char *OMemUsageFirstToLast( contextStructure )
                         OSearchContext *contextStructure;
*/
#ifdef LINT_ARGS
extern    char *OMemUsageFirstToLast( OSearchContext * );
#else
extern    char *OMemUsageFirstToLast();
#endif

/* function OMemAdrChange - notification of address change of an object buffe

     oldAdr is the old address of the buffer
     newAdr is the new address of the buffer

     Caller supplies the contextStructure to quickly identify directory entry
     whose buffer has been changed.  If contextStructure is not current, a
     directory search for the appropriate entry will be made.

     Returns 0 if oldAdr was found and directory updated
     else returns -1.

extern    int  OMemAdrChange( oldAdr, newAdr, contextStructure )
                    char           *oldAdr, *newAdr;
                    OSearchContext contextStructure;
*/
#ifdef LINT_ARGS
extern    int  OMemAdrChange( char *, char *, OSearchContext );
#else
extern    int  OMemAdrChange();
#endif

/* function OMemForfeitTotalSize - function forfeits memory cached objects
     until the specified byteCount has been forfeited.

     Returns number of bytes forfeited.

extern    Wordt     OMemForfeitTotalSize( byteCount )
                         Wordt     byteCount;
*/
#ifdef LINT_ARGS
extern    Wordt     OMemForeitTotalSize( Wordt );
#else
extern    Wordt     OMemForfeitTotalSize();
#endif

#endif
/* end of omemunit.in */
