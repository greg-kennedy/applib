/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/*  objscanu.in  6/18/86 10:40  */
/*   
     file:          objscanu.in
     author:        Michael L. Gordon
     date:          16-June-1986
     facility: Object Processor

     Description:

          Definition of the object scanner unit interface of the object
          processor.

     History:
*/
#ifndef objscanuDEF
#define objscanuDEF      1

/* Definition of the functional interface for object specific functions.
   Object specific functions are invoked with the following arguments:

   Note: the referencerObjid will be set to (Objectid *) 0
   if there is no referencing object.

   On entry, the object stream is positioned past the object header.

   The function returns 0 if OK; other return codes to be defined.

int   D_<objtype>( curObjid, curStream, curObjsize, referencerObjid,
                    referencerStream )
          Objectid       *curObjid;
          Objstream      curStream;
          int                 curObjsize;
          Objectid       *referencerObjid;
          Objstream      referencerStream;
*/

/* Definition of the functional interface for segment specific functions.
   Segment specific functions are invoked with the following arguments:

   On entry the stream is positioned past the ST and SL fields of the 
   segment to any data.  segsize is set to the number of bytes remaining
   in the segment; that is, segment total size - the number of bytes
   occupied by the ST and SL fields.

   The function returns 0 if OK; other return codes to be defined.

   If the segment references an object and the referenced object should
   be processed, the function sets the following:
     referencedObjid          is set to the id of the object to process
     processReference    is set to 0  ( the field defaults to non-zero )

int   D_<segtype>( curObjid, curStream, segsize, referencedObjid,
                    processReference )
          Objectid       *curObjid;
          Objstream      curStream;
          int                 segsize;
          Objectid       *referencedObjid;
          int                 *processReference;
*/

/* public function Obj_scan - scan the specified object.
   The object is accessed and type specific functions are invoked for
   objects and segments.
   A return code of 0 indicates success.
extern    int  Obj_scan( objid )
     Objectid       *objid;
*/


#endif    /* objscanuDEF not defined */

/* end of objscanu.in */

