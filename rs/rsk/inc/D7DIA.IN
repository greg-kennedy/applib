/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/***********************************************************************
*
*    File Name:     D7DIA.IN
*
************************************************************************
*    Description:   DIA header information for Driver 7
*
*
*    DATE      PROGRAMMER          REASON
* ----------------------------------------------------------------------
*    01/07/87  C. HSIEH       ORIGINAL
*
***********************************************************************/

/* offset of fields of FM0 in TX blk */
#define   B_FM0_HDRLEN        0    /* FM0 header length - 1 byte  */
#define   B_FM0_HDRTYPE       1    /* Header type       - 1 byte  */ 
#define B_FM0_FUNCODE         2    /* Function code     - 1 byte  */ 
#define B_FM0_DATAMODE        3    /* Data mode        - 1 byte  */ 
#define B_FM0_SID        4    /* Source ID        - 4 bytes */ 
#define B_FM0_LOGONNUM        8    /* Logon Sequence #  - 1 byte  */ 
#define B_FM0_MSGSEQNUM       9    /* Msg Sequence #    - 1 byte  */ 
#define B_FM0_DID        10   /* Destination ID    - 4 bytes */  
#define B_FM0_TXTLEN          14   /* Text length           - 2 bytes */ 


/* FM2 */
#define B_FM2_HDRLEN          16   /* FM2 header length - 1 byte  */ 
#define B_FM2_HDRTYPE    17   /* Header type           - 1 byte  */ 
#define B_FM2_NUMBLKS         18   /* Number of blocks  - 1 bytes */ 
#define B_FM2_CURBLK             19     /* Current block num - 1 byte  */ 


/* base offset for concatenated headers */
unsigned char  FM4_BASE;      /* base offset for FM4 */
unsigned char  FM9_BASE;      /* base offset for FM9 */ 
unsigned char  FM64_BASE;          /* base offset for FM64 */ 


/* FM4 */
#define B_FM4_HDRLEN       FM4_BASE     /* FM4 header length - 1 byte  */ 
#define B_FM4_HDRTYPE     FM4_BASE+1    /* Header type           - 1 byte  */ 
#define B_FM4_TTXUID      FM4_BASE+2    /* Trintex user ID   - 7 bytes */ 
#define B_FM4_EXTDATAMODE FM4_BASE+9    /* External data mode- 1 byte  */ 
#define B_FM4_CORELATID    FM4_BASE+10   /* Correlation ID    - 2 to 8 bytes */


/* FM9 */
#define B_FM9_HDRLEN       FM9_BASE     /* FM9 header length - 1 byte  */ 
#define B_FM9_HDRTYPE     FM9_BASE+1    /* Header type           - 1 byte  */ 
#define B_FM9_FCODE   FM9_BASE+2   /* Function code     - 1 bytes */ 
#define B_FM9_REASON      FM9_BASE+3    /* Reason code       - 1 byte  */ 
#define B_FM9_FLAG    FM9_BASE+4    /* Flag              - 1 byte */ 
#define B_FM9_TXTLEN      FM9_BASE+5     /* Text length       - 1 byte */ 

/* FM64 */
#define B_FM64_HDRLEN    FM64_BASE /* FM64 header length- 1 byte  */ 
#define   B_FM64_HDRTYPE  FM64_BASE+1   /* FM64 header type  - 1 byte  */ 
#define B_FM64_STATUS   FM64_BASE+2     /* FM64 status type  - 1 byte  */ 
#define B_FM64_DATAMODE FM64_BASE+3     /* FM64 data mode    - 1 byte  */ 
#define B_FM64_TXTLEN   FM64_BASE+4     /* FM64 text length  - 2 bytes */ 


typedef  struct     xfm4cntrl
{
     unsigned char  edmode;        /* External data mode */
     unsigned char  ttxuid[7];     /* TTX user ID */
     unsigned char   clen;         /* length of correlation ID */
     unsigned char  corrid[8];     /* Correlation ID */
}FM4PARM;

typedef  struct xcfm9cntrl
{
     unsigned char  fcode;         /* Function code */
     unsigned char   rcode;        /* Reason code */
     unsigned char  flag;          /* Flag */
     unsigned char  txtlen;        /* Text length */
}FM9PARM;

typedef  struct xfm64cntrl
{
     unsigned char  status;        /* Status */
     unsigned char   dmode;        /* Data mode */
     unsigned short  txtlen;       /* Text length */
     unsigned char   *text;          /* text itself C.H. 8/3/87 */
                         /* to hold the text on receive side */
}FM64PARM;

typedef  struct       xdiacntrl
{
     unsigned char  fm0_fcode;     /* FM0 function code */
     unsigned char  fm0_dmode;     /* FM0 data mode */
     unsigned char  did[4];        /* FM0 Destination ID */
     FM4PARM        *fm4info; /* FM4 information pointer */
     FM9PARM        *fm9info; /* FM9 information pointer */
     FM64PARM  *fm64info;     /* FM64 information pointer */
}DIAPARM;

/* TOCS header offset for OBJECT response */
#define   REQ_CODE  0         /* request code              */
#define   SEQ_NUM        1         /* number for end to end snc */
#define   BLK_NUMS  2         /* total # of blks in this msg */
#define BLK_INX          3         /* blk # */
#define OBJBLK_LEN      4          /* text length */

#define TOCSLEN     6         /* total length of TOCS */

#define OBJ_OFFSET  2         /* offset of object id in an */
                         /*  object request           */
