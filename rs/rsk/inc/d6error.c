/* RSK     = 1.1.4    LAST_UPDATE = 1.1.0    */
/**********************************************************************
*
* FILE NAME:    d6error.c
* DESCRIPTION:
*               Contains the module which defines DIA/TOCS interface layer
* error messages.
* NOTE: The constants which match these messages can be found in d7error.in.
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*     20 AUG. '86      M. Silfen          Original
*     8/28/87          L. A.              Add error 20
/**********************************************************************
*
*                 **** OMCM ERROR DECLARATIONS *****
*
**********************************************************************/
extern   d5err_print();
extern   null_rtn();

ECB  d5_err_table[] =
{
     "Staged object not found on disk", d5err_print,
     "Lost contact with Series I while fetching object", d5err_print,
     "Object does not exist on the TPF database", d5err_print,
     "File extension (from objectid) not in table",null_rtn,
     "No available space for DIA", d5err_print,
     "No available space for ICB", d5err_print,
     "Invalid DIA in received message", d5err_print,
     "No available space for xmit buffer", d5err_print,
     "Time out with send pending",d5err_print,
     "Time out with get pending",d5err_print,
     "Out of sequence message received",d5err_print,
     "Open file error",d5err_print,
     "Close file error",d5err_print,
     "Unlink file error",d5err_print,
     "Seeking file position error",d5err_print,
     "Time out while retrieving object",d5err_print,
     "No available space for coming message",d5err_print,
     "Invalid file extension in object ID", null_rtn,
     "Out of sequence block received",d5err_print,
     "Number of blocks expected in current header does not match previous hea
     "Out of records for interleaved objects",null_rtn,
     "Hardware problem reported on series 1",d5err_print
};

