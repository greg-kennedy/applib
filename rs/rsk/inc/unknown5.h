/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */

/* NAPLPS OPCODE DEFINES */

/* C0 Control Set */
#define NUL     0x0
#define SOH     0x1
#define STX     0x2
#define ETX     0x3
#define EOT     0x4
#define ENQ     0x5
#define ACK     0x6
#define BEL     0x7
#define APB     0x8
#define APF     0x9
#define APD     0xA
#define APU     0xB
#define CS      0xC
#define APR     0xD
#define SO      0xE    /* ENABLE PDI SET*/   
#define SI      0xF    /* ENABLE PRIMARY CHAR SET */
#define DLE     0x10
#define DC1     0x11
#define DC2     0x12
#define DC3     0x13
#define DC4     0x14
#define NAK     0x15
#define SYN     0x16
#define ETB     0x17
#define CAN     0x18
#define SS2     0x19
#define SDC     0x1A
#define ESC     0x1B    /* ESCAPE - ENABLES C1 SET */
#define APS     0x1C
#define SS3     0x1D
#define APH     0x1E
#define NSR     0x1F

/* C1 Control Set */
#define DEF_MACRO       0x40
#define DEFP_MACRO      0x41
#define DEFT_MACRO      0x42
#define DEF_DRCS        0x43
#define DEF_TEXTURE     0x44
#define END_DEF         0x45
#define REPEAT          0x46
#define REPEAT_TO_EOL   0x47
#define REVERSE_VIDEO   0x48
#define NORMAL_VIDEO    0x49
#define SMALL_TEXT      0x4A
#define MED_TEXT        0x4B
#define NORMAL_TEXT     0x4C
#define DOUBLE_HEIGHT   0x4D
#define BLINK_START     0x4E
#define DOUBLE_SIZE     0x4F
#define PROTECT         0x50
#define EDC1            0x51
#define EDC2            0x52
#define EDC3            0x53
#define EDC4            0x54
#define WORD_WRAP_ON    0x55
#define WORD_WRAP_OFF   0x56
#define SCROLL_OM       0x57
#define SCROLL_OF       0x58
#define UNDERLINE_START 0x59
#define UNDERLINE_STOP  0x5A
#define FLASH_CURSOR    0x5B
#define STEADY_CURSOR   0x5C
#define CURSOR_OFF      0x5D
#define BLINK_STOP      0x5E
#define UNPROTECT       0x5F

/* PDI Set */
#define RESET               0x20
#define DOMAIN              0x21
#define TEXT                0x22
#define TEXTURE             0x23
#define POINT_SET_ABS       0x24
#define POINT_SET_REL       0x25
#define POINT_ABS           0x26
#define POINT_REL           0x27
#define LINE_ABS            0x28
#define LINE_REL            0x29
#define SET_LINE_ABS        0x2A
#define SET_LINE_REL        0x2B
#define ARC_OUTLINED        0x2C
#define ARC_FILLED          0x2D
#define SET_ARC_OUTLINED    0x2E
#define SET_ARC_FILLED      0x2F
#define RECT_OUTLINED       0x30
#define RECT_FILLED         0x31 
#define SET_RECT_OUTLINED   0x32 
#define SET_RECT_FILLED     0x33 
#define POLY_OUTLINED       0x34 
#define POLY_FILLED         0x35 
#define SET_POLY_OUTLINED   0x36 
#define SET_POLY_FILLED     0x37 
#define NAPLPS_FIELD        0x38 
#define INCR_POINT          0x39 
#define INCR_LINE           0x3A 
#define INCR_POLY_FILLED    0x3B 
#define SET_COLOR           0x3C 
#define WAIT                0x3D 
#define SELECT_COLOR        0x3E 
#define BLINK               0x3F 
