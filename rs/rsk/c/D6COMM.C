/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*          **** PACKET/BLOCK INTERFACE LAYER ****
*
*********************************************************************
*
* FILE NAME:   D6COMM.C
* 
* DESCRIPTION: Contains routines to handle the interface between the
* packet oriented link level and block oriented DIA/TOCS level for
* send and receive.
* 
*    DATE     PROGRAMMER REASON
* --------------------------------------------------------------------
*    10/12/86    L. Wheeler   ORIGINAL
*    4/10/87       C. H. Timing interval in timeout changed
*    8/28/87       L. A. Use OMCM fatal error constants
*    1/07/88       S. S. Changed SLEEP(1) to SLEEP(2) in pop_char()
*                   to fix a TMK bug "temporarily".
*
**********************************************************************/

#include <stdio.h>
#include <memory.h>
#include <d7error.in>

/* add for definition of "IBC" - 11/6/87 DMM */
#include "rsystem.h"

/* remove Comm Manager dependent code - 11/6/87 DMM */
/* #include "icb.h" */


/* define BUF_SIZ        3072 C. H. */
#define SET         1
#define CLEAR       0

/* external Logical Operating System routines */
extern unsigned char *GET_MEMORY();
extern ICB *DEQ_TASK();                 /* was "struct icb *"  11/6/87 DMM */
extern int FREE_MEMORY();
extern void PREEMPT();
extern int SLEEP();
extern unsigned char debug;
extern void fatal_error();


#if THIS_IS_A_COMMENT_BLOCK        /* DON'T #define this !!! */

11/6/87 DMM

The following functions (in SMCMINTF.C) hide the Comm Manager
dependent structure of a struct lkdbk from this file. The
actual synopses of these functions are as follows:

(although they are declared using (void *), which can be
assigned to ANY pointer type, the actual types being dealt
with here are (struct lkdbk *) and (struct pkt *))

struct lkdbk *icb_u_rxpk(icbp);         /* returns (struct lkdbk *) from */
ICB *icbp;                   /* (ICB *) sent by Comm Manager */

struct pkt *lbk_nxpk(lp);          /* return "nxpk" field of an lkdbk */
struct lkdbk *lp;

void set_lbk_nxpk(lp, nxval);      /* set "nxpk" field of an lkdbk */
struct lkdbk *lp;            /* to "nxval" */
struct pkt *nxval;

int lbk_pklen(lp);           /* return "pklen" field of an lkdbk */
struct lkdbk *lp;

unsigned char *lbk_byte(lp);      /* return address of "byte" field */
struct lkdbk *lp;            /* of an lkdbk */

send_msg_to_cm(bufp, len);        /* send a buffer to Comm Manager */
unsigned char *bufp;              /* for transmission */

#endif                       /* THIS_IS_A_COMMENT_BLOCK */

void *icb_u_rxpk(ICB *);
void *lbk_nxpk(void *);
void set_lbk_nxpk(void *, void *);
int lbk_pklen(void *);
unsigned char *lbk_byte(void *);
int send_msg_to_cm(char *, int);

#define RCVE   5              /* receive data ICB          */

/*statics known to this module only */
/***
static unsigned char fifo[BUF_SIZ];
static unsigned char *fifo_in = &fifo[0];
***/

static unsigned short fifo_ct = 0 ;
static int data_lth;

/* static unsigned char *data_ptr;  */
static void *curr_pkt = NULL;      /* was "struct lkdbk *"   11/6/87  DMM */
static void *next_pkt;             /* was "struct pkt *" 11/6/87 DMM */
static int out_cnt = 0;
static unsigned char *bufp;

static void *HLD_BLK[4] = { NULL, NULL , NULL, NULL }; /* was "struct lkdbk */
static unsigned short    OUT_INDEX = 0 ;
static unsigned short    IN_INDEX = 0 ;
       void *FIFO_PKT = NULL;      /* was "struct lkdbk *"   11/6/87  DMM */
static void *FIFO_NXT_PKT = NULL;  /* was "struct lkdbk *"   11/6/87  DMM */
       unsigned short    FIFO_OFFSET = 0 ;    /*RDC made this public 7/10/87*/
static unsigned short    SET_FIFO = 0 ;
unsigned char *FIFO_OUT = NULL;

/* publics */
unsigned short TX_CNT, DATA_LEN;
unsigned char NEW_BLK;
/*****
unsigned char *FIFO_TOP = &fifo[0];
unsigned Char *FIFO_END = &fifo[BUF_SIZ - 1];
unsigned char *FIFO_OUT = &fifo[0];
******/
unsigned char *TXBUF;

/*unsigned short TIMVAL;       C.H. 7/21/87  */
extern unsigned int SM_TIMER ;     /* C.H. 7/21/87      */

unsigned char TIMEOUT = 0;

SEND_COM()
{
     int i;
     unsigned char *tmp_ptr;
     
     bufp = GET_MEMORY((unsigned long)TX_CNT);
     if (bufp == NULL)
          fata1_error(D5_PROC, XMIT_BUF_OUT_OF_MEMORY);

     tmp_ptr = bufp;
     
     memcpy( tmp_ptr, TXBUF, TX_CNT );   /* C. H. 3/25/87 */
     
     send_msg_to_cm(bufp, TX_CNT);       /* 11/6/87  DMM */
}


/**** Comments and modifications by cdp 1/12/87 */
/* RCV_COM:  Collect packets sent by Communications Manager.  Packets will
     be in order.  This routine "wakes up" when CM sends an ICB
     with a packet pointer.   This packet in turn points to the
     next packet, which points to the next packet, etc. until
     a null packet pointer is found.     This signals the end of
     the chain.  A fresh incoming ICB signals the start of a new
     chain.    Pending ICB's are ignored until the current chain
     is exhausted.
          Each successive call to this routine collects at most 1
     packet; on exit, the global curr_pkt variable points to the next
     packet in this chain to be collected.
          Return the # of bytes collected.
 */
int RCV_COM() /* called by driver loop in D6OMCM and by POP_CHAR() */
{
     ICB *icbp;
     int i;
     
     /* If next packet in chain has arrived from Comm Mgr, squirrel
        away its data, free the packet, and point to the next
        packet in the chain.
     */
     /* 11/6/87  DMM */
     if( (curr_pkt != NULL) && ((next_pkt= lbk_nxpk(curr_pkt)) != NULL) )
     {
          data_lth= lbk_pklen(next_pkt) + 1; /* 11/6/87  DMM */
          curr_pkt= next_pkt;           /* 11/6/87 DMM */
          fifo_ct += data_lth;
     }
 /* Otherwise, see if an ICB has arrived from Comm Mgr.      If so, begin
     a new chain and collect the first packet.  The only valid
     ICB message is ReCeiVE.
     */
     else if( (icbp= DEQ_TASK()) != NULL )
     {
          if(icbp->ifunc != RCVE)       /* 11/6/87  DMM */
#if DEBUG
               fprintf(stdprn, "RCV_COM: DEQ gave invalid msg\n\r");
#else
               ;
#endif
          else{
              if (FIFO_PKT == NULL)
               FIFO_PKT = icb_u_rxpk(icbp);  /* 11/6/87  DMM */
              else
               set_lbk_nxpk(curr_pkt, icb_u_rxpk(icbp));    /* 11/6/87  DMM */

              curr_pkt = icb_u_rxpk(icbp);   /* 11/6/87  DMM */

              data_lth= lbk_pklen(curr_pkt) + 1;  /* 11/6/87  DMM */

               /* copy_data(); */
                fifo_ct += data_lth;

          }
          
          FREE_MEMORY(icbp);
          
     } else data_lth=0;
     
     return( data_lth);
}

unsigned char POP_CHAR()
{
     int   i, j;
     unsigned char byte;
     
     byte = 0; /* initialize returned byte to null */
     TIMEOUT = CLEAR;
     
     /* we have not gotten a new byte yet */
     if ( fifo_ct == 0  )
     {
          RCV_COM(); /* first check message queue */
                   /* C.H. 7/21/87         */
          i = (SM_TIMER/2) * 10;   /* convert timeout value to LOS*/
                         /* sleep units             */

#ifdef PH
phook(33);
#endif

          /* while ((FIFO_OUT == fifo_in) && (i-- > 0))  */
          while ( ( fifo_ct == 0 ) && ( i-- > 0 ) )                /* if nothing */
          {
               SLEEP(2); /* Changed from 1 to 2 to fix a TMK
                              bug       */
#ifdef PH
phook(50);
#endif
               RCV_COM(); /* and then check it again */
          }
#ifdef PH
phook(34);
#endif

    }
    
    if ( fifo_ct != 0   )
    {
      if (FIFO_OFFSET > lbk_pklen(FIFO_PKT))      /* 11/6/87  DMM */
         {
          FIFO_NXT_PKT = lbk_nxpk(FIFO_PKT);      /* 11/6/87  DMM */
          FREE_MEMORY(FIFO_PKT);
          FIFO_PKT = FIFO_NXT_PKT ;
          FIFO_OFFSET = 0;
         }
      
      /* get next byte in FIFO_PKT */
      byte = *( lbk_byte(FIFO_PKT) + FIFO_OFFSET );    /* 11/6/87  DMM */
      
      /* decrement count of bytes in bfr */
      fifo_ct--;
      
      FIFO_OFFSET++;
      
     }
     else /* even after waiting we still didn't get one */
     {
#if DEBUG
          fprintf(stdprn, "timeout in d6comm\n\r");
#endif
          TIMEOUT = SET;
     }
     
     return(byte);

}

unsigned char POP_N_CHK()
{
     unsigned char  byte;
     
     int   i, j;
     byte = POP_CHAR();
     out_cnt++;
     if (DATA_LEN == out_cnt)
     (
         out_cnt = O;
         NEW_BLK = SET;
     }
     return (byte);
}


unsigned short GET_CNT()
{
     return (fifo_ct);
}

badscom() {}
